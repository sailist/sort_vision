import time
from tkinter import *
from sort_method import *
import random
import tkinter
import tkinter.colorchooser
import tkinter.filedialog
def run_sort(tosave = False,fname = None):
    sort_string = sort_input.get()
    sort_string=sort_string.replace("，",",").replace("；",",").replace(";",",").replace(" ",",")
    spl_str = sort_string.split(",")
    sort_data = [float(spl_str[i]) for i in range(0,len(spl_str)) if len(spl_str[i])>0]
    to_sort = sort_index.get()
    
    repeat = userepeat.get()
    repeat_delay = int(repeat_delay_var.get())
    interval = int(interval_var.get())

    colors = [bt["bg"] for bt in color_bt]

    start_sort(to_sort,sort_data,repeat = repeat,repeat_delay=repeat_delay,interval=interval,colors = colors,tosave=tosave,fname = fname,dpi = int(dpi_var.get()))

def creat_randomlist(rangelr,num):
    randomdata = range(rangelr[0],rangelr[1])  
    randomlist = random.sample(randomdata,num)
    return randomlist


def gene_random(data = None):
    if data == None:
        random_list = creat_randomlist([0,100],random.randint(10,20))
        sort_str.set( ",".join([str(i) for i in random_list]))
    else:
        data = data.replace("，",",")
        data = data.replace("；",",")
        data = data.replace(";",",")
        data = data.replace(" ",",")
        sort_str.set(data)

def choose_file_input():
    #fpath = filedialog.askopenfilename(filetypes=[("bmp格式","bmp")])
    fpath = tkinter.filedialog.askopenfilename(filetypes=[("txt格式","txt")])
    with open(fpath,'r') as f:
        lines = f.readlines()
        strs = "".join(lines)
        gene_random(strs)
def choose_color(event,bt=None):
    thecolor = tkinter.colorchooser.askcolor()
    if thecolor is not None:
        bt["bg"] = thecolor[1]
    #print(event,thecolor)

def save_sort():
    print(save_index.get())
    nowtime = time.strftime('%y%m%d_%H%M%S',time.localtime(time.time()))
    defaultfilename = sort_name[sort_index.get()].replace(" ","_")+"_"+nowtime
    if save_index.get() == 1:
        fname = tkinter.filedialog.asksaveasfilename(initialfile = "[index]"+defaultfilename,filetypes=[("图片格式","jpg")])
        fname = fname.replace(".jpg","")+".jpg"
        if fname:
            run_sort(True,fname)#这里要改成文件夹形式打开
    elif save_index.get() == 2:
        fname = tkinter.filedialog.asksaveasfilename(initialfile  = defaultfilename,filetypes=[("视频格式","mp4")])
        fname = fname.replace(".mp4","")+".mp4"
        run_sort(True,fname)
    print(fname)


def ini_gui():
    #define global variable
    global sort_input
    global sort_index
    global userepeat
    global interval_var
    global repeat_delay_var
    global dpi_var
    global color_bt
    global save_index
    global sort_name
    
    #creat main gui
    mg = Tk()
    mg.title("排序演示工具")
    
    #use frame to split window
    frames = []
    frames.append(Frame())

    #define input filed ui
    #label
    input_field_label = Label(mg,text = "数据输入与排序选择区",borderwidth = 2)
    input_field_label.pack(side = TOP,anchor = NW)

    #input field
    sort_str = StringVar()
    sort_input = Entry(mg,textvariable = sort_str)
    sort_input.pack(fill = X)
    random_list = creat_randomlist([0,200],10)
    sort_str.set( ",".join([str(i) for i in random_list]))

    #this button is to input data from local file
    file_input_bt = Button(mg,text = "Choose file",command = choose_file_input)
    file_input_bt.pack(side = RIGHT,anchor = NE)

    #this button is to create random datas to sort
    random_bt = Button(mg,text = "Random",command = gene_random)
    random_bt.pack(side = RIGHT,anchor = NE)

    #define Radiobutton
    sort_index = IntVar()
    sort_index.set(0)
    sort_index_value = range(0,4)
    sort_name = ["Insert sort","Select sort","Base sort","Quick sort"]
    radios = []
    for i in sort_index_value:
        radios = radios + [Radiobutton(mg,text=sort_name[i],variable = sort_index,value = i)]
        if i == 3:
            radios[i].pack(side =LEFT,anchor = NW)
        else:
            radios[i].pack(side = LEFT,anchor = NW)
    #mg.pack(ipady = 5,fill = X)
    frames.append(Frame())
    #the first frame is loaded

    
    color_bt = []
    #define diy animation field
    #label
    diy_field_label = Label(mg,text = "自定义动画区")
    diy_field_label.pack(side = TOP,anchor = NW)

    #data-show color can use this button to change
    choose_nosort_bt = Button(mg,text = "no sort color",command = lambda : choose_color(1,choose_nosort_bt))
    choose_nosort_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_nosort_bt]
    choose_sorting_bt = Button(mg,text = "sorting color",command = lambda : choose_color(2,choose_sorting_bt))
    choose_sorting_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_sorting_bt]
    choose_sorted_bt = Button(mg,text = "sorted color",command = lambda : choose_color(3,choose_sorted_bt))
    choose_sorted_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_sorted_bt]
    choose_bound_bt = Button(mg,text = "bound line color",command = lambda : choose_color(4,choose_bound_bt))
    choose_bound_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_bound_bt]

    #define default color variable
    default_color = ['#add8e6','#a7d51e','#ffa500','#d9513c']
    for i in range(0,len(color_bt)):
        color_bt[i]["bg"] = default_color[i]

    #this Checkbutton is to decide whether the animation can repeat
    userepeat = IntVar()
    isrepeat = Checkbutton(mg,variable = userepeat,text = "repeat")
    userepeat.set(1)
    isrepeat.pack(side = RIGHT)

    #mg.pack(ipady = 3,fill = X)
    frames.append(Frame())
    #the second frame is loaded

    interval_label = Label(mg,text = "interval(ms)")
    interval_label.pack(anchor = NW,side =LEFT)

    #use to set the interval(ms)
    interval_var = StringVar()
    interval_set_input = Entry(mg,textvariable = interval_var)
    interval_var.set("100")
    interval_set_input.pack(padx = 5,side = LEFT)



    repeat_delay_label = Label(mg,text = "repeat delay(ms)")
    repeat_delay_label.pack(anchor = NW,side = LEFT)

    repeat_delay_var = StringVar()
    repeat_delay_input = Entry(mg,textvariable = repeat_delay_var)
    repeat_delay_var.set("100")
    repeat_delay_input.pack(padx = 5,side = LEFT,fill = X,expand = YES)

    #mg.pack(fill = X)
    frames.append(Frame())
    #second frame is loaded

    #define animation output field
    #label
    Label(mg,text = "动画输出区").pack(side = TOP,anchor = NW)

    run_bt = Button(mg,text = "run animation",command = run_sort)
    run_bt.pack(anchor = NE,side = RIGHT)

    save_bt = Button(mg,text = "save animation",command = save_sort)
    save_bt.pack(anchor = NE,side = RIGHT)

    save_index = IntVar()
    Radiobutton(mg,text="video",variable = save_index,value = 2).pack(side = RIGHT)
    #Radiobutton(mg,text="html",variable = save_index,value = 3).pack(side = RIGHT)
    Radiobutton(mg,text="images",variable = save_index,value = 1).pack(side = RIGHT)
    save_index.set(2)

    Label(mg,text = "清晰度(dpi)").pack(side = LEFT)

    dpi_var = StringVar()
    dpi_set_input = Entry(mg,width = 10,textvariable = dpi_var)
    dpi_var.set("120")
    dpi_set_input.pack(padx = 5,side = LEFT)
    #third frame is loaded
    
    mg.resizable(0,0)#set the window usresizable
    #start the gui
    mg.mainloop()


if __name__ =='__main__':
    ini_gui()
