import re
import time
import logging
from tkinter import *
from sort_method import *
import random
import tkinter
import tkinter.colorchooser
import tkinter.filedialog
def log(*strings):
    logger.info(" ".join(strings)) 


def run_sort(tosave = False,fname = None):
    log("start output")
    
    sort_string = sort_input.get()
    sort_string = re.sub("[^\d^-]+",",",sort_string)
    log("data:",sort_string)
    spl_str = sort_string.split(",")
    sort_data = [int(spl_str[i]) for i in range(0,len(spl_str)) if len(spl_str[i])>0]
    to_sort = sort_index.get()
    

    interval = int(interval_var.get())
    log("frame interval",str(interval))

    repeat = userepeat.get()
    log("repeat animation",str(repeat))
    
    repeat_delay = int(repeat_delay_var.get())
    if repeat:
        log("repeat delay",str(repeat_delay))
    
    colors = [bt["bg"] for bt in color_bt]
    
    log("data colors",",".join(colors))



    start_sort(to_sort,sort_data,repeat = repeat,repeat_delay=repeat_delay,interval=interval,colors = colors,tosave=tosave,fname = fname,dpi = int(dpi_var.get()))

def creat_randomlist(rangelr,num):
    
    randomdata = range(rangelr[0],rangelr[1])  
    randomlist = random.sample(randomdata,num)
    log("created random list:" + ",".join([str(num) for num in randomlist]))
    return randomlist


def gene_random(data = None):
    if data == None:
        random_list = creat_randomlist([-150,-50],random.randint(5,15))
        sort_str.set( ",".join([str(i) for i in random_list]))
        log("set data in the input field:",",".join([str(i) for i in random_list]))
    else:
        data = re.sub("[^\d]+",",",data)
        spldata = data.split(",")
        data = ",".join([sdata for sdata in spldata if len(sdata)>0])
        sort_str.set(data)
        log("set data in the input field:",data)
    
def choose_file_input():
    #fpath = filedialog.askopenfilename(filetypes=[("bmp格式","bmp")])
    log("choose file")
    fpath = tkinter.filedialog.askopenfilename(filetypes=[("txt格式","txt")])
    
    if len(fpath) == 0:
        log("path empty")
        return
    log("choose file with path:",fpath)
    with open(fpath,'r',encoding='UTF-8',errors = "replace") as f:
        lines = f.readlines()
        strs = "".join(lines)
        gene_random(strs)
def choose_color(event,bt=None):
    thecolor = tkinter.colorchooser.askcolor()
    if thecolor is not None:
        bt["bg"] = thecolor[1]
    #print(event,thecolor)

def save_sort():
    print(save_index.get())
    nowtime = time.strftime('%y%m%d_%H%M%S',time.localtime(time.time()))
    defaultfilename = sort_name[sort_index.get()].replace(" ","_")+"_"+nowtime
    if save_index.get() == 1:
        fname = tkinter.filedialog.asksaveasfilename(initialfile = "[index]"+defaultfilename,filetypes=[("图片格式","jpg")])
        fname = fname.replace(".jpg","")+".jpg"
        if len(fname)>4:
            run_sort(True,fname)#这里要改成文件夹形式打开
    elif save_index.get() == 2:
        fname = tkinter.filedialog.asksaveasfilename(initialfile  = defaultfilename,filetypes=[("视频格式","mp4")])
        fname = fname.replace(".mp4","")+".mp4"
        if len(fname)>4:
            run_sort(True,fname)
    print(fname)


def ini_gui():
    log("starting the program...")
    #define global variable
    global sort_input
    global sort_index
    global userepeat
    global interval_var
    global repeat_delay_var
    global dpi_var
    global color_bt
    global save_index
    global sort_name
    global sort_str
    #creat main gui
    mg = Tk()
    mg.title("排序演示工具")

    log("creating first frame...")
    #use frame to split window
    frames = []
    frames.append(Frame())

    #define input filed ui
    #label
    input_field_label = Label(frames[0],text = "数据输入与排序选择区",borderwidth = 2)
    input_field_label.pack(side = TOP,anchor = NW)

    #input field
    sort_str = StringVar()
    sort_input = Entry(frames[0],textvariable = sort_str)
    sort_input.pack(fill = X)
    random_list = creat_randomlist([0,200],10)
    sort_str.set( ",".join([str(i) for i in random_list]))

    #this button is to input data from local file
    file_input_bt = Button(frames[0],text = "Choose file",command = choose_file_input)
    file_input_bt.pack(side = RIGHT,anchor = NE)

    #this button is to create random datas to sort
    random_bt = Button(frames[0],text = "Random",command = gene_random)
    random_bt.pack(side = RIGHT,anchor = NE)

    #define Radiobutton
    sort_index = IntVar()
    sort_index.set(0)
    sort_index_value = range(0,4)
    sort_name = ["Insert sort","Bubble sort","Base sort","Quick sort"]
    radios = []
    for i in sort_index_value:
        radios = radios + [Radiobutton(frames[0],text=sort_name[i],variable = sort_index,value = i)]
        if i == 3:
            radios[i].pack(side =LEFT,anchor = NW)
        else:
            radios[i].pack(side = LEFT,anchor = NW)
    frames[0].pack(ipady = 5,fill = X)
    frames.append(Frame())
    #the first frame is loaded

    
    color_bt = []
    #define diy animation field
    #label
    diy_field_label = Label(frames[1],text = "自定义动画区")
    diy_field_label.pack(side = TOP,anchor = NW)

    #data-show color can use this button to change
    choose_nosort_bt = Button(frames[1],text = "no sort color",command = lambda : choose_color(1,choose_nosort_bt))
    choose_nosort_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_nosort_bt]
    choose_sorting_bt = Button(frames[1],text = "sorting color",command = lambda : choose_color(2,choose_sorting_bt))
    choose_sorting_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_sorting_bt]
    choose_sorted_bt = Button(frames[1],text = "sorted color",command = lambda : choose_color(3,choose_sorted_bt))
    choose_sorted_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_sorted_bt]
    choose_bound_bt = Button(frames[1],text = "bound line color",command = lambda : choose_color(4,choose_bound_bt))
    choose_bound_bt.pack(side = LEFT)
    color_bt = color_bt + [choose_bound_bt]

    #define default color variable
    default_color = ['#add8e6','#a7d51e','#ffa500','#d9513c']
    for i in range(0,len(color_bt)):
        color_bt[i]["bg"] = default_color[i]

    #this Checkbutton is to decide whether the animation can repeat
    userepeat = IntVar()
    isrepeat = Checkbutton(frames[1],variable = userepeat,text = "repeat")
    userepeat.set(1)
    isrepeat.pack(side = RIGHT)

    frames[1].pack(ipady = 3,fill = X)
    log("creating second frame...")
    frames.append(Frame())
    #the second frame is loaded

    interval_label = Label(frames[2],text = "interval(ms)")
    interval_label.pack(anchor = NW,side =LEFT)

    #use to set the interval(ms)
    interval_var = StringVar()
    interval_set_input = Entry(frames[2],textvariable = interval_var)
    interval_var.set("100")
    interval_set_input.pack(padx = 5,side = LEFT)



    repeat_delay_label = Label(frames[2],text = "repeat delay(ms)")
    repeat_delay_label.pack(anchor = NW,side = LEFT)

    repeat_delay_var = StringVar()
    repeat_delay_input = Entry(frames[2],textvariable = repeat_delay_var)
    repeat_delay_var.set("100")
    repeat_delay_input.pack(padx = 5,side = LEFT,fill = X,expand = YES)

    frames[2].pack(fill = X)
    frames.append(Frame())
    #second frame is loaded
    log("creating final frame...")
    #define animation output field
    #label
    Label(mg,text = "动画输出区").pack(side = TOP,anchor = NW)

    run_bt = Button(mg,text = "run animation",command = run_sort)
    run_bt.pack(anchor = NE,side = RIGHT)

    save_bt = Button(mg,text = "save animation",command = save_sort)
    save_bt.pack(anchor = NE,side = RIGHT)

    save_index = IntVar()
    Radiobutton(mg,text="video",variable = save_index,value = 2).pack(side = RIGHT)
    #Radiobutton(mg,text="html",variable = save_index,value = 3).pack(side = RIGHT)
    Radiobutton(mg,text="images",variable = save_index,value = 1).pack(side = RIGHT)
    save_index.set(2)

    Label(mg,text = "清晰度(dpi)").pack(side = LEFT)

    dpi_var = StringVar()
    dpi_set_input = Entry(mg,width = 10,textvariable = dpi_var)
    dpi_var.set("120")
    dpi_set_input.pack(padx = 5,side = LEFT)
    #third frame is loaded
    
    mg.resizable(0,0)#set the window usresizable
    #start the gui
    mg.mainloop()
    log("ui is all created")


if __name__ =='__main__':
    global logger
    logging.basicConfig(level = logging.INFO,format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s',filename = "./log.txt",filemode = "a")
    logger = logging.getLogger("main_gui")
    ini_gui()
