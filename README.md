# sort_vision
数据结构课设...
用python写的四种排序方法的可视化过程演示，
使用tkinter制作界面，matplotlib绘制演示过程动画，并且加入了输出图片集/视频到本地的功能
以及内置了ffmpeg作为视频编码器，关于matplotlib的animation的save()函数的使用可以看一下我写的这篇博客
[matplotlib animation动画保存(save函数)详解](http://blog.csdn.net/sailist/article/details/79502007)   