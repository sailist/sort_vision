import logging
import os
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
def log(*strings):
    logger.info(" ".join(strings)) 

nosortcolor = 'red'
sortingcolor = 'green'
sortokcolor = 'blue'
noshowcolor = 'cyan'

def getlist(sq,index):
    return [sq[i] for i in index]

def removelist(sq,index):
    sub = [sq[i] for i in range(0,len(sq)) if not (i in index)]
    return [sq[i] for i in range(0,len(sq)) if not (i in index)]

def draw_quick_text(*strings):
    lines = []
    nowaxis = plt.axis()
    lines.append(plt.text(nowaxis[0], nowaxis[3], " ".join(strings), ha='left', va='top', fontsize=12))
    
    return lines
    
def drawbase(data,count):
    lines = []
    datafig = plt.subplot(2, 1, 1)
    index = range(len(data))
    indexx = [index,index]
    
    datafig.set_xticks([])
    datafig.set_yticks([])
    datafig.set_xlim(-1, len(data))
    if data!= []:
        per = (max(data)-offset)/4
        datafig.set_ylim(0, (max(data)-offset) *1.5)
        dataa = [[offset]*len(index),[data[i]-offset+1 for i in range(0,len(data))]]
        lines = lines + datafig.plot(indexx,dataa,lw = 9,color = nosortcolor)
    
    
    for i in range(len(data)):
        lines.append(datafig.text(i, data[i] -offset + 2, '%.0f' % data[i], ha='center', va='bottom', fontsize=12))

    index = [i+offset for i in range(0,len(count)) if count[i]>0]
    count = [count[i] for  i in range(0,len(count)) if count[i]>0]
    countfig = plt.subplot(2, 1, 2)
    #index = range(len(count))
    indexx = [index,index]
    dataa = [[offset]*len(index),count]
    lines = lines + countfig.plot(indexx,dataa,lw = 2,color = sortingcolor)
    if count != []:
        countfig.set_ylim(0, max(count) + 6)
    #print(index,count)
    countfig.set_xticks(data)
    for i in range(len(count)):
        if count[i] > 0:
            lines.append(countfig.text(index[i], count[i]+0.1, '%.0f' % count[i], ha='center', va='bottom', fontsize=12))
    return lines


def inifig(data,noshow=[]):#初始化坐标，初始化文本，如果已经开始排序，那么排序的那个数字跳过
    #offset = min(0,min(data))
    
    lines = []
    if len(noshow) == 1:
        lines.append(plt.text(len(data), (max(data)-offset)*1.4, 'sort:'+str(data[noshow[0]]), ha='right', va='top', fontsize=12))
    for i in range(len(data)):
        if i in noshow:
            continue
        ydata =  data[i]-offset + 2
        lines.append(plt.text(i, ydata, '%.0f' % data[i], ha='center', va='bottom', fontsize=12))
    plt.xticks([])
    plt.yticks([])
    plt.ylim(0,(max(data)-offset)*1.4)
    plt.xlim(-1,len(data))
    return lines
     
def drawbar(data,nosort=None,sorting=[],sortok=[],noshow=[]):
    if nosort == None:
        nosort = range(len(data))
    #offset = min(0,min(data))
    lines = []
    lines = lines + inifig(data,noshow)
    data = [data[i]-offset for i in range(0,len(data))]
    
    yeindex = [nosort,nosort]
    yedata = [[offset]*len(nosort),getlist(data,nosort)]
    
    blindex = [sortok,sortok]
    bldata = [[offset]*len(sortok),getlist(data,sortok)]

    whindex = [noshow,noshow]
    whdata = [[offset]*len(noshow),getlist(data,noshow)]

    grindex = [sorting,sorting]
    grdata = [[offset]*len(sorting),getlist(data,sorting)]
    

    lines = lines + plt.plot(yeindex, yedata,lw=9, color=nosortcolor)
    lines = lines + plt.plot(blindex, bldata, lw=9, color=sortokcolor)
    lines = lines + plt.plot(whindex, whdata, lw=10, color=noshowcolor)
    lines = lines + plt.plot(grindex, grdata, lw=10, color=sortingcolor)
    return lines
     

def insert_sort(data):
    # print(data)
    lines = []
    onelines = []
    #inifig(data)

    onelines.append(drawbar(data))
    
    lines = lines + onelines
    index = range(len(data))
    for i in range(0, len(data)):
        nosort = index[i:]
        sorting =[i]
        sortok = index[:i]
        noshow = []
        onelines = []
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        
        lines = lines + onelines
        for j in range(i, 0, -1):
            if data[j] < data[j - 1]:
                nosort = index[i+1:]
                sorting =[]
                sortok = index[:i+1]
                noshow = [j]
                onelines = []
                onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
                lines = lines + onelines
                data[j], data[j - 1] = data[j - 1], data[j]

                nosort = index[i+1:]
                sorting =[]
                sortok = index[:i+1]
                noshow = [j-1]
                onelines = []
                onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
                lines = lines + onelines
        nosort = index[i+1:]
        sorting =[]
        sortok = index[:i+1]
        noshow = []
        onelines = []
        #inifig(data)
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        lines = lines + onelines
    return lines
    #断点：要排序的，已经排好序的，还没排序的

def bubble_sort(data):
    lines = []
    onelines = []
    index = range(len(data))
    onelines.append(drawbar(data,index,[],[],[]))
    lines = lines + onelines
    
    for i in range(0,len(data)):
        for j in range(0,len(data)-i-1):
            
            nosort = getlist(index,range(0,len(data)-i))
            sorting =[j]
            sortok = index[-i:]
            if i == 0:
                sortok = []
            noshow = []
            onelines = []
            onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
            lines = lines + onelines
            #print(data[j],j)
            if data[j]>data[j+1]:
                data[j],data[j+1]=data[j+1],data[j]
                nosort = getlist(index,range(0,len(data)-i))
                sorting =[j+1]
                sortok = index[-i:]
                if i == 0:
                    sortok = []
                noshow = []
                onelines = []
                onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
                lines = lines + onelines
        nosort = getlist(index,range(0,len(data)-i))
        sorting =[]
        sortok = index[-i:]
        if i == 0:
            sortok = []
        noshow = []
        onelines = []
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        lines = lines + onelines

    nosort = []
    sorting =[]
    sortok = index
    noshow = []
    onelines = []
    onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
    lines = lines + onelines
    return lines

def base_sort(data):
    data = [int(data[i]) for i in range(0,len(data))]
    lines = []
    minnum = min(data)
    #offset = min(0,minnum)
    maxnum = max(data)
    count = [0]*int(maxnum-offset+1)#画初始data，count
    lines.append(drawbase(data,count))
    for i in range(0,len(data)):
        count[data[i]-offset] = count[data[i]-offset] + 1#更新count
        lines.append(drawbase(data[i:],count))
    data = [[i+offset]*count[i] for i in range(0,len(count))]
    newdata = []
    lines.append(drawbase(newdata,count))
    
    for i in range(0,len(data)):
        newdata = newdata + data[i]#更新data
        if len(data[i])>0:
            count[i] = 0
        if len(newdata)>0 and len(data[i])>0:
            lines.append(drawbase(newdata,count))
    #print(count)
    lines.append(drawbase(newdata,count))
    
    return lines

#bound should be an array that len = 2, the left line and the right line
def draw_line(data,bound):

    leftindex = [bound[0]-0.5,bound[0]-0.5]
    leftdata = [0, max(data)]
    rightindex = [bound[1]+0.5, bound[1]+0.5]
    rightdata = [0 ,max(data)]
    lines = []
    lines = lines + inifig(data)
    lines = lines + plt.plot(leftindex,leftdata,lw = 1,color = noshowcolor)
    lines = lines + plt.plot(rightindex,rightdata,lw = 1,color = noshowcolor)
    return lines
    

def quick_sort(data, low, high):
    
    lines = []
    index = range(len(data))
    
    
    nosort,sorting,sortok,noshow,onelines =index,[],[],[],[]
    onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
    onelines[0] = onelines[0] + draw_line(data,[low,high]) +draw_quick_text("the bound index is",str(low),"to",str(high))
    lines = lines + onelines

    i = low
    j = high
    if i >= j:
        nosort,sorting,sortok,noshow,onelines = index,[],[],[],[]
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
        lines = lines + onelines
        return lines
    
    key = data[i]
    
    nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[i,j],[],[],[]
    onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
    onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
    lines = lines + onelines
    
    while i < j:
        while i < j and data[j] >= key:
            
            nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[i],[j],[],[]
            onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
            onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
            lines = lines + onelines
            
            j = j - 1

            nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[i],[j],[],[]
            onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
            onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
            lines = lines + onelines

        data[i],data[j] = data[j],data[i]
                                
        nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[j],[i],[],[]
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
        lines = lines + onelines
                            
        while i < j and data[i] <= key:
            nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[j],[i],[],[]
            onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
            onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
            lines = lines + onelines
            
            i = i + 1
            
            nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[j],[i],[],[]
            onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
            onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
            lines = lines + onelines
            
        data[j],data[i]= data[i],data[j]  

        nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[i],[j],[],[]
        onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
        onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
        lines = lines + onelines

    data[i] = key

    nosort,sorting,sortok,noshow,onelines = removelist(index,[i,j]),[i,j],[],[],[]
    onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
    onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
    lines = lines + onelines
    
    lines = lines + quick_sort(data, low, i - 1)
    lines = lines + quick_sort(data, j + 1, high)

    nosort,sorting,sortok,noshow,onelines = index,[],[],[],[]
    onelines.append(drawbar(data,nosort,sorting,sortok,noshow))
    onelines[0] = onelines[0] + draw_line(data,[low,high])+ draw_quick_text("the bound index is",str(low),"to",str(high))
    lines = lines + onelines
    
    return lines




def start_sort(sort_index,data,interval=500, repeat_delay=600,repeat = False,colors=['#add8e6','#a7d51e','#ffa500','#d9513c'],dpi = 300,tosave = False,fname = None,notplay = False):
    global logger
    logging.basicConfig(level = logging.INFO,format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s',filename = "./log.txt",filemode = "a")
    logger = logging.getLogger("sort_method")       

    log("define figure")
    fig = plt.figure(dpi = dpi)
    global nosortcolor
    global sortingcolor
    global sortokcolor
    global noshowcolor
    nosortcolor = colors[0]
    sortingcolor = colors[1]
    sortokcolor = colors[2]
    noshowcolor = colors[3]


    global offset
    offset = min(0,min(data))

    if sort_index == 0:
        log("choose insert sort,creating frames...")
        ims = insert_sort(data)
    elif sort_index == 1:
        log("choose bubble sort,creating frames...")
        ims = bubble_sort(data)
    elif sort_index == 2:
        log("choose base sort,creating frames...")
        ims = base_sort(data)
    elif sort_index == 3:
        log("choose quick sort,creating frames...")
        ims = quick_sort(data,0,len(data)-1)
    log("data'frames creat over,put frames in the animation")
    im_ani = animation.ArtistAnimation(fig, ims, interval=interval, repeat_delay=repeat_delay,repeat = repeat,
                                   blit=True)
    
    
    if notplay == True:
        return fig,im_ani
    if not tosave:
        log("output mood: play directly")
        plt.show()
    else:
        start_save(fname,fig,[im_ani])
    

def start_save(fname,fig,all_anim):

    if fname:
        dirname,filename = os.path.split(fname)
        
        if os.path.exists(dirname) and os.path.isdir(dirname) :
            if not os.listdir(dirname):#目录为空
                pass
            else:#目录不为空
                dirname = os.path.join(dirname,filename.split(".")[0].replace("[index]",""))
                
                os.mkdir(dirname)
        log("output mood: to local file, the path:",dirname)
        fname = os.path.join(dirname,filename)
    
    if fname.endswith(".jpg"):
        i = 0
        log("output format : jpg")
        for anim in all_anim:
            anim._init_draw()
        for data in zip(*[a.new_saved_frame_seq() for a in all_anim]):
            for anim, d in zip(all_anim, data):
                anim._draw_next_frame(d, blit=False)
                fig.savefig(fname.replace('index',str(i)),dip = 600)
                i = i+1
                log("save the"+ str(i+1)+"th" + " image")
    elif fname.endswith(".mp4"):
        log("output format : mp4")
        if  os.path.exists("./ffmpeg/bin/ffmpeg.exe"):
            ffmpegpath = os.path.abspath("./ffmpeg/bin/ffmpeg.exe")
            matplotlib.rcParams["animation.ffmpeg_path"] = ffmpegpath
            writer = animation.FFMpegWriter()
            all_anim[0].save(fname,writer = writer)
            log("output over")
        else:
            logger.error("ffmpeg not found")
            raise FileNotFoundError("can't find ffmpeg")

    
if __name__ =='__main__':
    fig,ite = start_sort(1,[1,2,6,3],notplay = True)
    start_save(1123,fig,[ite])
